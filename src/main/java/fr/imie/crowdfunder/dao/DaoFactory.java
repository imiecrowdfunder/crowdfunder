package fr.imie.crowdfunder.dao;

import fr.imie.crowdfunder.dao.jpa.JpaCategoryDao;
import fr.imie.crowdfunder.dao.jpa.JpaProjectDao;
import fr.imie.crowdfunder.dao.jpa.JpaUserDao;
import fr.imie.crowdfunder.util.PersistenceManager;

public class DaoFactory {
	public static CategoryDao getCategoryDao() {
		return new JpaCategoryDao(PersistenceManager.getEntityManagerFactory());
	}

	public static ProjectDao getProjectDao() {
		return new JpaProjectDao(PersistenceManager.getEntityManagerFactory());
	}

	public static UserDao getUserDao() {
		return new JpaUserDao(PersistenceManager.getEntityManagerFactory());
	}
}
