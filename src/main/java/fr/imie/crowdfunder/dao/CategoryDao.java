package fr.imie.crowdfunder.dao;

import java.util.List;

import fr.imie.crowdfunder.entity.Category;

public interface CategoryDao {

	Category addCategory(Category category);

	Category findCategory(long categoryId);

	Category updateCategory(Category category);

	Category removeCategory(long categoryId);

	List<Category> getAllCategory();

}