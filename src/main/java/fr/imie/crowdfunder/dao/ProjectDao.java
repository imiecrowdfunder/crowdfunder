package fr.imie.crowdfunder.dao;

import java.util.List;

import fr.imie.crowdfunder.entity.Project;

public interface ProjectDao {

	Project addProject(Project project);

	Project findProject(long projectId);

	Project updateProject(Project project);

	Project removeProject(long projectId);

	List<Project> getAllProject();

	List<Project> getRecentProject();
}
