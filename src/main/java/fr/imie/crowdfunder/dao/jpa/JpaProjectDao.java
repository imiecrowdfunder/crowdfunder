package fr.imie.crowdfunder.dao.jpa;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import fr.imie.crowdfunder.dao.DaoFactory;
import fr.imie.crowdfunder.dao.ProjectDao;
import fr.imie.crowdfunder.entity.Category;
import fr.imie.crowdfunder.entity.Project;
import fr.imie.crowdfunder.entity.User;

public class JpaProjectDao extends JpaDao implements ProjectDao {
	public JpaProjectDao(EntityManagerFactory emf) {
		super(emf);
	}

	@Override
	public Project addProject(Project project) {
		EntityManager em = getEntityManagerFactory().createEntityManager();
		EntityTransaction transaction = em.getTransaction();

		try {
			transaction.begin();

			Category category = em.find(Category.class, project.getCategory().getCategoryId());
			project.setCategory(category);
			category.getListProject().add(project);

			List<User> listContributor = new ArrayList<User>();
			project.getListContributor().forEach(user -> {
				User u = em.find(User.class, user.getUserId());
				u.getListProjectContribute().add(project);
				listContributor.add(u);
			});
			project.setListContributor(listContributor);

			User creator = em.find(User.class, project.getUser().getUserId());
			creator.getListProjectCreated().add(project);
			project.setUser(creator);

			em.persist(project);
			em.flush();

			transaction.commit();

			if (project.getProjectId() != 0) {
				return project;
			} else {
				return null;
			}
		} finally {
			if (transaction.isActive())
				transaction.rollback();
			em.close();
		}
	}

	@Override
	public Project findProject(long projectId) {
		EntityManager em = getEntityManagerFactory().createEntityManager();

		try {
			Project project = em.find(Project.class, projectId);

			if (project != null) {
				return project;
			} else {
				return null;
			}
		} finally {
			em.close();
		}
	}

	@Override
	public Project updateProject(Project project) {
		EntityManager em = getEntityManagerFactory().createEntityManager();
		EntityTransaction transaction = em.getTransaction();

		try {
			transaction.begin();

			Project pOld = em.find(Project.class, project.getProjectId());
			pOld.setCategory(project.getCategory());
			pOld.setContributionActual(project.getContributionActual());
			pOld.setContributionExpected(project.getContributionExpected());
			pOld.setDateBegin(project.getDateBegin());
			pOld.setDateEnd(project.getDateEnd());
			pOld.setDescription(project.getDescription());
			pOld.setListContributor(project.getListContributor());
			pOld.setName(project.getName());

			em.merge(pOld);
			transaction.commit();

			Project pCurrent = DaoFactory.getProjectDao().findProject(project.getProjectId());

			if (pCurrent.equals(pOld)) {
				return project;
			} else {
				return null;
			}
		} finally {
			if (transaction.isActive())
				transaction.rollback();
			em.close();
		}

	}

	@Override
	public Project removeProject(long projectId) {
		EntityManager em = getEntityManagerFactory().createEntityManager();
		EntityTransaction transaction = em.getTransaction();

		try {
			transaction.begin();
			Project[] project = { em.find(Project.class, projectId) };
			Category category = em.find(Category.class, project[0].getCategory().getCategoryId());

			if (project[0] != null) {
				category.getListProject().remove(project[0]);
				em.merge(category);

				project[0].setCategory(null);

				project[0].getListContributor().forEach(user -> {
					User userContrib = em.find(User.class, user.getUserId());

					userContrib.getListProjectContribute().remove(project[0]);

				});

				project[0].setListContributor(null);

				User createUser = em.find(User.class, project[0].getUser().getUserId());
				createUser.getListProjectCreated().remove(project[0]);
				em.merge(createUser);

				project[0].setUser(null);

				em.merge(project[0]);

				em.remove(em.find(Project.class, project[0].getProjectId()));

				transaction.commit();

				return project[0];
			} else {
				return null;
			}
		} finally {
			if (transaction.isActive())
				transaction.rollback();
			em.close();
		}

	}

	@Override
	public List<Project> getAllProject() {
		EntityManager em = getEntityManagerFactory().createEntityManager();

		try {
			Query query = em.createQuery("SELECT p FROM Project AS p");

			@SuppressWarnings("unchecked")
			List<Project> listProject = query.getResultList();

			return listProject;
		} finally {
			em.close();
		}
	}

	@Override
	public List<Project> getRecentProject() {
		EntityManager em = getEntityManagerFactory().createEntityManager();

		try {
			Query query = em.createQuery("SELECT p FROM Project AS p ORDER BY dateBegin DESC").setMaxResults(3);

			@SuppressWarnings("unchecked")
			List<Project> listProject = query.getResultList();

			return listProject;
		} finally {
			em.close();
		}
	}

}
