package fr.imie.crowdfunder.dao.jpa;

import javax.persistence.EntityManagerFactory;

public abstract class JpaDao {
	private EntityManagerFactory emf;

	protected JpaDao(EntityManagerFactory emf) {
		this.emf = emf;
	}

	protected EntityManagerFactory getEntityManagerFactory() {
		return emf;
	}
}
