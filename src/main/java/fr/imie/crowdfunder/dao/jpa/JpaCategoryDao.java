package fr.imie.crowdfunder.dao.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import fr.imie.crowdfunder.dao.CategoryDao;
import fr.imie.crowdfunder.dao.DaoFactory;
import fr.imie.crowdfunder.entity.Category;

public class JpaCategoryDao extends JpaDao implements CategoryDao {
	public JpaCategoryDao(EntityManagerFactory emf) {
		super(emf);
	}

	@Override
	public Category addCategory(Category category) {
		EntityManager em = getEntityManagerFactory().createEntityManager();
		EntityTransaction transaction = em.getTransaction();

		try {
			transaction.begin();
			em.persist(category);
			em.flush();
			transaction.commit();

			if (category.getCategoryId() != 0) {
				return category;
			} else {
				return null;
			}
		} finally {
			if (transaction.isActive())
				transaction.rollback();
			em.close();
		}
	}

	@Override
	public Category findCategory(long categoryId) {
		EntityManager em = getEntityManagerFactory().createEntityManager();
		Category category = em.find(Category.class, categoryId);
		try {
			if (category != null) {
				return category;
			} else {
				return null;
			}
		} finally {
			em.close();
		}

	}

	@Override
	public Category updateCategory(Category category) {
		EntityManager em = getEntityManagerFactory().createEntityManager();
		EntityTransaction transaction = em.getTransaction();

		try {
			transaction.begin();

			Category cOld = DaoFactory.getCategoryDao().findCategory(category.getCategoryId());

			em.merge(category);
			transaction.commit();

			Category cCurrent = DaoFactory.getCategoryDao().findCategory(category.getCategoryId());

			if (!cCurrent.equals(cOld)) {
				return category;
			} else {
				return null;
			}
		} finally {
			if (transaction.isActive())
				transaction.rollback();
			em.close();
		}
	}

	@Override
	public Category removeCategory(long categoryId) {
		EntityManager em = getEntityManagerFactory().createEntityManager();
		EntityTransaction transaction = em.getTransaction();

		try {
			transaction.begin();

			Category[] category = { em.find(Category.class, categoryId) };
			if (category[0] != null) {
				category[0].getListProject().forEach(project -> {
					project.setCategory(null);
					em.merge(project);
				});

				category[0].setListProject(null);
				em.merge(category[0]);

				em.remove(category[0]);

				transaction.commit();

				return category[0];
			} else {
				return null;
			}
		} finally {
			if (transaction.isActive())
				transaction.rollback();
			em.close();
		}
	}

	@Override
	public List<Category> getAllCategory() {
		EntityManager em = getEntityManagerFactory().createEntityManager();

		try {
			Query query = em.createQuery("SELECT c FROM Category AS c");

			@SuppressWarnings("unchecked")
			List<Category> listCategory = query.getResultList();

			return listCategory;
		} finally {
			em.close();
		}
	}
}
