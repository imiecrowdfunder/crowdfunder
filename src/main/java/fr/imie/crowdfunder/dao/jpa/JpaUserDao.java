package fr.imie.crowdfunder.dao.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import fr.imie.crowdfunder.dao.DaoFactory;
import fr.imie.crowdfunder.dao.UserDao;
import fr.imie.crowdfunder.entity.User;

public class JpaUserDao extends JpaDao implements UserDao {

	public JpaUserDao(EntityManagerFactory emf) {
		super(emf);
	}

	@Override
	public User addUser(User user) {
		EntityManager em = getEntityManagerFactory().createEntityManager();
		EntityTransaction transaction = em.getTransaction();

		try {
			transaction.begin();
			em.persist(user);
			em.flush();
			transaction.commit();

			if (user.getUserId() != 0) {
				return user;
			} else {
				return null;
			}
		} finally {
			if (transaction.isActive())
				transaction.rollback();
			em.close();
		}
	}

	@Override
	public User findUser(long userId) {
		EntityManager em = getEntityManagerFactory().createEntityManager();
		try {
			User user = em.find(User.class, userId);

			if (user != null) {
				return user;
			} else {
				return null;
			}
		} finally {
			em.close();
		}
	}

	@Override
	public User updateUser(User user) {
		EntityManager em = getEntityManagerFactory().createEntityManager();
		EntityTransaction transaction = em.getTransaction();

		try {
			transaction.begin();
			em.merge(user);
			transaction.commit();

			User u = DaoFactory.getUserDao().findUser(user.getUserId());

			if (user.equals(u)) {
				return user;
			} else {
				return null;
			}
		} finally {
			if (transaction.isActive())
				transaction.rollback();
			em.close();
		}

	}

	@Override
	public User removeUser(long userId) {
		EntityManager em = getEntityManagerFactory().createEntityManager();
		EntityTransaction transaction = em.getTransaction();

		try {
			transaction.begin();
			User[] user = { em.find(User.class, userId) };

			if (user[0] != null) {
				if (user[0].getListProjectContribute().size() > 0) {
					user[0].getListProjectContribute().forEach(project -> {
						project.getListContributor().remove(user[0]);
						em.merge(project);
					});

					user[0].setListContribuProject(null);
				}

				if (user[0].getListProjectCreated().size() > 0) {
					user[0].getListProjectCreated().forEach(project -> {
						project.setUser(null);
						em.merge(project);
					});

					user[0].setListProjectCreated(null);
				}

				em.merge(user[0]);
				em.remove(user[0]);

				transaction.commit();

				return user[0];
			} else {
				return null;
			}
		} finally {
			if (transaction.isActive())
				transaction.rollback();
			em.close();
		}

	}

	@Override
	public List<User> getAllUser() {
		EntityManager em = getEntityManagerFactory().createEntityManager();
		try {
			Query query = em.createQuery("SELECT u FROM User AS u");

			@SuppressWarnings("unchecked")
			List<User> uList = query.getResultList();

			return (uList != null) ? uList : null;
		} finally {
			em.close();
		}
	}
}
