package fr.imie.crowdfunder.dao;

import java.util.List;

import fr.imie.crowdfunder.entity.User;

public interface UserDao {

	User addUser(User user);

	User findUser(long userId);

	User updateUser(User user);

	User removeUser(long userId);

	List<User> getAllUser();
}
