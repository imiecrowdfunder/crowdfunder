package fr.imie.crowdfunder.filter;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.imie.crowdfunder.entity.User;

@WebFilter(filterName = "authentificateFilter", servletNames = { "ShowUserServlet", "AddCategoryServlet",
		"AddProjectServlet", "EditCategoryServlet", "EditProjectServlet", "EditUserServlet", "ShowUserServlet",
		"ListUserServlet", "RemoveCategoryServlet", "RemoveProjectServlet", "RemoveUserServlet", "LogoutServlet",
		"AddContributionServlet" })
public class AuthenticateFilter implements Filter {

	@SuppressWarnings("unused")
	private ServletContext context;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		this.context = filterConfig.getServletContext();
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		User currentUser = (User) req.getSession().getAttribute("currentUser");

		List<Boolean> urlException = Arrays.asList(new Boolean[] { req.getRequestURI().matches("^/CrowdFunder/$"),
				req.getRequestURI().matches(".*login$"), req.getRequestURI().matches(".*register$"),
				req.getRequestURI().matches(".*home$"), req.getRequestURI().matches(".*listProject$"),
				req.getRequestURI().matches(".*showProject"), req.getRequestURI().matches(".*rest/project/allProject$"),
				req.getRequestURI().matches(".*rest/project/find/[0-9]+$"), req.getRequestURI().matches(".*rest/user$"),
				// DEBUG
				// req.getRequestURI().matches(".*rest/project/update/[0-9]+$"),
				// req.getRequestURI().matches(".*rest/project$"),
				// req.getRequestURI().matches(".*rest/user/update/[0-9]+$$"),
				// req.getRequestURI().matches(".*rest/user/find/[0-9]+$"),
		});
		// DEBUG
		// System.out.println(req.getRequestURI());

		// Fake Data
		// Date dateBegin = null;
		//
		// try {
		// dateBegin = new SimpleDateFormat("yyyy-MM-dd").parse("2017-07-25
		// 00:00:00.0");
		// } catch (ParseException e) {
		// e.printStackTrace();
		// }
		// User user = new
		// User("GEF","Fabbro","Geoffrey","gef56150@gmail.com","GEF");
		// DaoFactory.getUserDao().addUser(user);
		//
		// Category category = new Category("test");
		// DaoFactory.getCategoryDao().addCategory(category);
		//
		// User userCreate = new User("test", "aa","aa","aa.aa@aa.fr","aa");
		// User userCreate2 = new User("lol", "aa","aa","aa.aa@aa.fr","aa");
		// userCreate = DaoFactory.getUserDao().addUser(userCreate);
		// userCreate2 = DaoFactory.getUserDao().addUser(userCreate2);
		//
		// Project project = new Project(category, "lol", userCreate, "test",
		// dateBegin, 50.0F);
		// project = DaoFactory.getProjectDao().addProject(project);
		//
		// User userContrib = new User("pigeon", "bb", "bb", "bb.bb@bb.fr",
		// "bb");
		// DaoFactory.getUserDao().addUser(userContrib);
		//
		// project.getListContributor().add(userContrib);
		// project.setContributionActual(10.0F);
		//
		// DaoFactory.getProjectDao().updateProject(project);
		if (req.getRequestURI().matches(".*(css|jpg|png|gif|js)$")) {
			chain.doFilter(request, response);
		} else if (urlException.contains(true)) {
			chain.doFilter(request, response);
		} else if (currentUser == null) {
			req.getRequestDispatcher("login.jsp").forward(req, (HttpServletResponse) response);
		} else {
			chain.doFilter(request, response);
		}
	}
}
