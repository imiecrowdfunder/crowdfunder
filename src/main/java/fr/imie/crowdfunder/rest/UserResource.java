package fr.imie.crowdfunder.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import fr.imie.crowdfunder.dao.DaoFactory;
import fr.imie.crowdfunder.entity.User;

@Path("user")
public class UserResource {
	@GET
	@Path("find/{userId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUser(@PathParam("userId") Long userId) {
		User user = DaoFactory.getUserDao().findUser(userId);

		if (user != null) {
			return Response.ok(user).build();
		} else {
			return Response.notModified().build();
		}
	}

	@PUT
	@Path("update/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateUser(@PathParam("userId") String userId, User user) {
		User u = DaoFactory.getUserDao().updateUser(user);

		if (user.equals(u)) {
			return Response.ok(user).build();
		} else {
			return Response.notModified().build();
		}
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response handleUser(User user) {
		User u = DaoFactory.getUserDao().addUser(user);

		if (u != null) {
			return Response.ok(user).build();
		} else {
			return Response.notModified().build();
		}
	}
}
