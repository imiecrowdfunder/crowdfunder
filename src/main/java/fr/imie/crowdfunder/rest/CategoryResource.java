//package fr.imie.crowdfunder.rest;
//
//import java.util.Collection;
//
//import javax.ws.rs.Consumes;
//import javax.ws.rs.DELETE;
//import javax.ws.rs.GET;
//import javax.ws.rs.POST;
//import javax.ws.rs.PUT;
//import javax.ws.rs.Path;
//import javax.ws.rs.PathParam;
//import javax.ws.rs.Produces;
//import javax.ws.rs.core.MediaType;
//import javax.ws.rs.core.Response;
//
//import fr.imie.crowdfunder.dao.DaoFactory;
//import fr.imie.crowdfunder.entity.Category;
//import fr.imie.crowdfunder.entity.Project;
//
//
//@Path("category")
//public class CategoryResource {
//	@GET
//	@Path("allCategory")
//	@Produces(MediaType.APPLICATION_JSON)
//	public Response getAllCategory() {
//		Collection<Category> listCategory = DaoFactory.getCategoryDao().getAllCategory();
//		
//		if (listCategory != null) {
//			return Response.ok(listCategory).build();
//		} else {
//			return Response.notModified().build();
//		}
//	}
//
//	@GET
//	@Path("find/{categoryId}")
//	@Produces(MediaType.APPLICATION_JSON)
//	public Response getCategory(@PathParam("categoryId") Long categoryId) {
//		Category category = DaoFactory.getCategoryDao().findCategory(categoryId);
//		
//		if (category != null) {
//			return Response.ok(category).build();
//		} else {
//			return Response.notModified().build();
//		}
//	}
//
//	@DELETE
//	@Path("remove/{categoryId}")
//	@Produces(MediaType.APPLICATION_JSON)
//	public Response removeProduct(@PathParam("categoryId") Long categoryId) {
//		Category category = DaoFactory.getCategoryDao().removeCategory(categoryId);
//		
//		if (category != null) {
//			return Response.ok(category).build();
//		} else {
//			return Response.notModified().build();
//		}
//	};
//
//	@PUT
//	@Path("update/{categoryId}")
//	@Produces(MediaType.APPLICATION_JSON)
//	@Consumes(MediaType.APPLICATION_JSON)
//	public Response updateCategory(@PathParam("categoryId") Long categoryId, Category category) {
//		Category cOld = DaoFactory.getCategoryDao().findCategory(Long.valueOf(categoryId));
//		Category cCurrent = DaoFactory.getCategoryDao().updateCategory(category);
//
//		if (cOld != null) {
//			if (!cOld.equals(cCurrent)) {
//				return Response.ok(cCurrent).build();
//			} 
//		}
//		
//		return Response.notModified().build();
//	}
//
//	@POST
//	@Consumes(MediaType.APPLICATION_JSON)
//	@Produces(MediaType.APPLICATION_JSON)
//	public Response handleCategory(Category category) {
//		Category c = DaoFactory.getCategoryDao().addCategory(category);
//		
//		if (c != null) {			
//			return Response.ok(category).build();
//		} else {
//			return Response.notModified().build();
//		}
//	}
//
//}
