package fr.imie.crowdfunder.rest;

import java.util.Collection;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import fr.imie.crowdfunder.dao.DaoFactory;
import fr.imie.crowdfunder.entity.Project;

@Path("project")
public class ProjectResource {

	@GET
	@Path("allProject")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllProject() {
		Collection<Project> listProject = DaoFactory.getProjectDao().getAllProject();

		if (listProject != null) {
			return Response.ok(listProject).build();
		} else {
			return Response.notModified().build();
		}
	}

	@GET
	@Path("find/{projectId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getProject(@PathParam("projectId") Long projectId) {
		Project project = DaoFactory.getProjectDao().findProject(projectId);

		if (project != null) {
			return Response.ok(project).build();
		} else {
			return Response.notModified().build();
		}
	}

	@PUT
	@Path("update/{projectId}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateProject(@PathParam("projectId") String projectId, Project project) {
		Project pOld = DaoFactory.getProjectDao().findProject(Long.valueOf(projectId));
		Project pCurrent = DaoFactory.getProjectDao().updateProject(project);

		if (pOld != null) {
			if (!pOld.equals(pCurrent)) {
				return Response.ok(project).build();
			}
		}

		return Response.notModified().build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response handleCategory(Project project) {
		Project p = DaoFactory.getProjectDao().addProject(project);

		if (p != null) {
			return Response.ok(project).build();
		} else {
			return Response.notModified().build();
		}
	}
}
