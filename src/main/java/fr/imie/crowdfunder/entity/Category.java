package fr.imie.crowdfunder.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "category")
@XmlRootElement
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "categoryId", scope = Category.class)
public class Category extends BaseEntity implements Serializable {
	private static final long serialVersionUID = -2381992333771775256L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "category_id")
	private long categoryId;

	private String name;

	@OneToMany(mappedBy = "category", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JsonBackReference
	@Column(name = "list_project")
	private Collection<Project> listProject;

	public Category() {
	}

	@JsonCreator
	public Category(@JsonProperty("categoryId") long categoryId, @JsonProperty("name") String name) {
		this.categoryId = categoryId;
		this.name = name;
	}

	public Category(String name) {
		this.name = name;
	}

	public long getCategoryId() {
		return this.categoryId;
	}

	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}

	public String getName() {
		if (this.name == null) {
			this.name = new String();
		}

		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Collection<Project> getListProject() {
		if (this.listProject == null) {
			this.listProject = new ArrayList<Project>();
		}

		return this.listProject;
	}

	public void setListProject(Collection<Project> listProject) {
		this.listProject = listProject;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null) {
			if (this.hashCode() == obj.hashCode()) {
				return true;
			}
		}

		return false;
	}

	@Override
	public int hashCode() {
		int result;

		result = this.getListProject().hashCode();
		result = super.SEED * (int) this.getCategoryId();
		result = super.SEED * result + this.getName().hashCode();

		return result;
	}
}
