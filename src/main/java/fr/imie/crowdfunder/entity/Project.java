package fr.imie.crowdfunder.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "project")
@XmlRootElement
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "projectId", scope = Project.class)
public class Project extends BaseEntity implements Serializable {
	private static final long serialVersionUID = -2227127974705232363L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "project_id")
	private long projectId;

	@ManyToOne
	@JoinColumn(name = "category_fk", nullable = true)
	@JsonManagedReference(value = "project-category")
	private Category category;

	private String name;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "user_fk", nullable = true)
	@JsonManagedReference(value = "user")
	private User user;

	private String description;

	@Column(name = "contribution_actual")
	private float contributionActual;

	@Temporal(TemporalType.DATE)
	@Column(name = "date_begin")
	private Date dateBegin;

	@Temporal(TemporalType.DATE)
	@Column(name = "date_end")
	private Date dateEnd;

	@Column(name = "contribution_expected")
	private float contributionExpected;

	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "list_contributor")
	@JsonManagedReference(value = "list-contributor")
	@Column(name = "list_contributor")
	private Collection<User> listContributor;

	public Project() {

	}

	public Project(Category category, String name, User user, String description, Date dateBegin,
			Float contributionExpected) {
		this.category = category;
		this.name = name;
		this.user = user;
		this.description = description;
		this.dateBegin = dateBegin;
		this.contributionExpected = contributionExpected;
	}

	@JsonCreator
	public Project(@JsonProperty("projectId") long projectId, @JsonProperty("category") Category category,
			@JsonProperty("name") String name, @JsonProperty("user") User user,
			@JsonProperty("description") String description,
			@JsonProperty("contributionActual") float contributionActual, @JsonProperty("dateBegin") Date dateBegin,
			@JsonProperty("dateEnd") Date dateEnd, @JsonProperty("contributionExpected") float contributionExpected,
			@JsonProperty("listContributor") Collection<User> listContributor) {
		this.projectId = projectId;
		this.category = category;
		this.name = name;
		this.user = user;
		this.description = description;
		this.contributionActual = contributionActual;
		this.contributionExpected = contributionExpected;
		this.dateBegin = dateBegin;
		this.dateEnd = dateEnd;
		this.listContributor = listContributor;
	}

	public long getProjectId() {
		return this.projectId;
	}

	public void setProjectId(long projectId) {
		this.projectId = projectId;
	}

	public Category getCategory() {
		if (this.category == null) {
			this.category = new Category();
		}

		return this.category;
	}

	public void setCategory(Category projectCategory) {
		this.category = projectCategory;
	}

	public String getName() {
		if (this.name == null) {
			this.name = new String();
		}

		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public User getUser() {
		if (this.user == null) {
			this.user = new User();
		}

		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getDescription() {
		if (this.description == null) {
			this.description = new String();
		}

		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public float getContributionActual() {
		return this.contributionActual;
	}

	public void setContributionActual(Float contributionActual) {
		this.contributionActual = contributionActual;
	}

	public Date getDateBegin() {
		if (this.dateBegin == null) {
			this.dateBegin = new Date();
		}

		return this.dateBegin;
	}

	public void setDateBegin(Date dateBegin) {
		this.dateBegin = dateBegin;
	}

	public Date getDateEnd() {
		if (this.dateEnd == null) {
			this.dateEnd = new Date();
		}

		return this.dateEnd;
	}

	public void setDateEnd(Date dateEnd) {
		this.dateEnd = dateEnd;
	}

	public float getContributionExpected() {
		return this.contributionExpected;
	}

	public void setContributionExpected(Float contributionExpected) {
		this.contributionExpected = contributionExpected;
	}

	public Collection<User> getListContributor() {
		if (this.listContributor == null) {
			this.listContributor = new ArrayList<User>();
		}

		return this.listContributor;
	}

	public void setListContributor(Collection<User> listContributor) {
		this.listContributor = listContributor;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null) {
			if (this.hashCode() == obj.hashCode()) {
				return true;
			}
		}

		return false;
	}

	@Override
	public int hashCode() {
		int result;

		result = this.getListContributor().hashCode();
		result = super.SEED * (int) this.getProjectId();
		result = super.SEED * result + this.getCategory().hashCode();
		result = super.SEED * result + this.getName().hashCode();
		result = super.SEED * result + this.getUser().hashCode();
		result = super.SEED * result + this.getDescription().hashCode();
		result = super.SEED * result + (int) this.getContributionActual();
		result = super.SEED * result + this.getDateBegin().hashCode();
		result = super.SEED * result + this.getDateEnd().hashCode();
		result = super.SEED * result + (int) this.getContributionExpected();

		return result;
	}
}
