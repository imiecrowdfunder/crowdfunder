package fr.imie.crowdfunder.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "user")
@XmlRootElement
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "userId", scope = User.class)
public class User extends BaseEntity implements Serializable {
	private static final long serialVersionUID = -8381514623382834447L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_id")
	private long userId;

	private String pseudo;

	private String firstName;

	private String lastName;

	private String email;

	private String password;

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
	@JsonBackReference(value = "list-project-created")
	@Column(name = "list_project_created")
	private Collection<Project> listProjectCreated;

	@ManyToMany(mappedBy = "listContributor", cascade = CascadeType.ALL)
	@JsonBackReference(value = "list-project-contribute")
	@Column(name = "list_project")
	private Collection<Project> listProjectContribute;

	@JsonCreator
	public User(@JsonProperty("userId") long userId, @JsonProperty("pseudo") String pseudo,
			@JsonProperty("firstName") String firstName, @JsonProperty("lastName") String lastName,
			@JsonProperty("email") String email, @JsonProperty("password") String password) {
		this.userId = userId;
		this.pseudo = pseudo;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
	}

	public User() {
	}

	public User(String pseudo, String firstName, String lastName, String email, String password) {
		this.pseudo = pseudo;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
	}

	public long getUserId() {
		return this.userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getPseudo() {
		if (this.pseudo == null) {
			this.pseudo = new String();
		}

		return this.pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public String getFirstName() {
		if (this.firstName == null) {
			this.firstName = new String();
		}

		return this.firstName;
	}

	public void setFirstName(String firstname) {
		this.firstName = firstname;
	}

	public String getLastName() {
		if (this.lastName == null) {
			this.lastName = new String();
		}

		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		if (this.email == null) {
			this.email = new String();
		}

		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		if (this.password == null) {
			this.password = new String();
		}

		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Collection<Project> getListProjectCreated() {
		if (this.listProjectCreated == null) {
			this.listProjectCreated = new ArrayList<Project>();
		}

		return this.listProjectCreated;
	}

	public void setListProjectCreated(Collection<Project> listProjectCreated) {
		this.listProjectCreated = listProjectCreated;
	}

	public Collection<Project> getListProjectContribute() {
		if (this.listProjectContribute == null) {
			this.listProjectContribute = new ArrayList<Project>();
		}

		return this.listProjectContribute;
	}

	public void setListContribuProject(Collection<Project> listProjectContribute) {
		this.listProjectContribute = listProjectContribute;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null) {
			if (this.hashCode() == obj.hashCode()) {
				return true;
			}
		}

		return false;
	}

	@Override
	public int hashCode() {
		int result;

		result = this.getListProjectContribute().hashCode();
		result = super.SEED * this.getListProjectCreated().hashCode();
		result = super.SEED * (int) this.getUserId();
		result = super.SEED * result + (int) this.getPseudo().hashCode();
		result = super.SEED * result + this.getFirstName().hashCode();
		result = super.SEED * result + this.getLastName().hashCode();
		result = super.SEED * result + this.getEmail().hashCode();
		result = super.SEED * result + this.getPassword().hashCode();

		return result;
	}
}