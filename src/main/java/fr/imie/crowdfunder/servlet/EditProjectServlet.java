package fr.imie.crowdfunder.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.imie.crowdfunder.dao.DaoFactory;
import fr.imie.crowdfunder.entity.Category;
import fr.imie.crowdfunder.entity.Project;

@WebServlet(urlPatterns = "/editProject")
public class EditProjectServlet extends HttpServlet {

	private static final long serialVersionUID = -432311735767520248L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String response = "listProject";

		if (req.getParameter("projectId") != null) {
			Long projectId = Long.parseLong(req.getParameter("projectId"));
			Project project = DaoFactory.getProjectDao().findProject(projectId);
			List<Category> listCategory = DaoFactory.getCategoryDao().getAllCategory();
			req.setAttribute("project", project);
			req.setAttribute("listCategory", listCategory);
			req.setAttribute("currentCategory", project.getCategory());
			response = "editProject.jsp";
		}

		req.getRequestDispatcher(response).forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String responseView = "login.jsp";
		Category category = null;
		Project project = null;

		try {
			project = DaoFactory.getProjectDao().findProject(Long.valueOf(req.getParameter("projectId")));
			project.setDescription(req.getParameter("description"));
			category = DaoFactory.getCategoryDao().findCategory(Long.valueOf(req.getParameter("categoryId")));
			project.setCategory(category);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (project != null && category != null) {
			DaoFactory.getProjectDao().updateProject(project);
			responseView = "listProject";
		}

		req.getRequestDispatcher(responseView).forward(req, resp);
	}
}
