package fr.imie.crowdfunder.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.imie.crowdfunder.dao.DaoFactory;

@WebServlet(urlPatterns = "/removeProject")
public class RemoveProjectServlet extends HttpServlet {

	private static final long serialVersionUID = -2196027340222964326L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String responseServlet = "home";

		try {
			long projectId = Long.parseLong(req.getParameter("projectId"));
			DaoFactory.getProjectDao().removeProject(projectId);
			responseServlet = "listProject";
		} catch (Exception e) {
			e.printStackTrace();
		}
		req.getRequestDispatcher(responseServlet).forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		this.doGet(req, resp);
	}
}
