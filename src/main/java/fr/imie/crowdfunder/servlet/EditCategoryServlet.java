package fr.imie.crowdfunder.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.imie.crowdfunder.dao.DaoFactory;
import fr.imie.crowdfunder.entity.Category;

@WebServlet(urlPatterns = "/editCategory")
public class EditCategoryServlet extends HttpServlet {

	private static final long serialVersionUID = -7592170972495666715L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher("editCategory.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String responseView = "login.jsp";
		Category category = null;
		
		try {
			category = new Category();
			category.setCategoryId(((Category) req.getSession().getAttribute("currentUser")).getCategoryId());
			category.setName(req.getParameter("name"));
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (category != null) {
			DaoFactory.getCategoryDao().updateCategory(category);
			responseView = "listCategory.jsp";
		}

		req.getRequestDispatcher(responseView).forward(req, resp);
	}

}
