package fr.imie.crowdfunder.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.imie.crowdfunder.dao.DaoFactory;
import fr.imie.crowdfunder.entity.Project;

@WebServlet(urlPatterns = "/showProject")
public class ShowProjectServlet extends HttpServlet {

	private static final long serialVersionUID = -6236828350163686444L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Project project = null;
		String response = "home";

		try {
			long projectId = Long.parseLong(req.getParameter("projectId"));
			project = DaoFactory.getProjectDao().findProject(projectId);
			req.setAttribute("project", project);
			req.setAttribute("listContributor", project.getListContributor());

			response = "showProject.jsp";
		} catch (Exception e) {
			e.printStackTrace();
		}

		req.getRequestDispatcher(response).forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		this.doGet(req, resp);
	}

}
