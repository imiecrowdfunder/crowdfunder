package fr.imie.crowdfunder.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.imie.crowdfunder.dao.DaoFactory;
import fr.imie.crowdfunder.entity.Project;

@WebServlet(urlPatterns = "/home")
public class HomeServlet extends HttpServlet {

	private static final long serialVersionUID = 7822613553280830469L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<Project> listProject = DaoFactory.getProjectDao().getRecentProject();

		if (listProject == null) {
			listProject = new ArrayList<Project>();
		}

		req.setAttribute("listProject", listProject);

		req.getRequestDispatcher("home.jsp").forward(req, resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		this.doGet(req, resp);
	}
}
