package fr.imie.crowdfunder.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.imie.crowdfunder.dao.DaoFactory;
import fr.imie.crowdfunder.entity.User;

@WebServlet(urlPatterns = "/editUser")
public class EditUserServlet extends HttpServlet {

	private static final long serialVersionUID = 2000187655276668806L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.getRequestDispatcher("editProfile.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String responseView = "login.jsp";
		User user = null;

		try {
			user = new User();
			user.setUserId(((User) req.getSession().getAttribute("currentUser")).getUserId());
			user.setPseudo(req.getParameter("pseudo"));
			user.setFirstName(req.getParameter("firstName"));
			user.setLastName(req.getParameter("lastName"));
			user.setPassword(req.getParameter("password"));
			user.setEmail(req.getParameter("email"));
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (user != null) {
			User u = DaoFactory.getUserDao().updateUser(user);

			if (u != null) {
				req.getSession().setAttribute("currentUser", u);
				responseView = "showProfile.jsp";
			}
		}

		req.getRequestDispatcher(responseView).forward(req, resp);
	}
}
