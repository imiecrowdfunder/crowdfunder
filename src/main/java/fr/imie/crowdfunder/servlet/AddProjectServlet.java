package fr.imie.crowdfunder.servlet;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.imie.crowdfunder.dao.DaoFactory;
import fr.imie.crowdfunder.entity.Category;
import fr.imie.crowdfunder.entity.Project;
import fr.imie.crowdfunder.entity.User;

@WebServlet(urlPatterns = "/addProject")
public class AddProjectServlet extends HttpServlet {

	private static final long serialVersionUID = -3878364098811077813L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<Category> listCategory = DaoFactory.getCategoryDao().getAllCategory();
		req.getSession().setAttribute("listCategory", listCategory);

		req.getRequestDispatcher("addProject.jsp").forward(req, resp);

	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String response = "login.jsp";
		Project project = new Project();

		try {
			project = new Project();
			Date currentDate = new Date();
			project.setName(req.getParameter("name"));
			project.setDescription(req.getParameter("description"));
			project.setUser((User) req.getSession().getAttribute("currentUser"));
			project.setDateBegin(currentDate);
			project.setContributionExpected(Float.parseFloat(req.getParameter("contributionExpected")));
			project.setContributionActual(0F);
			project.setDateEnd((Date) req.getAttribute("dateEnd"));

			Category category = DaoFactory.getCategoryDao().findCategory(Long.valueOf(req.getParameter("categoryId")));
			project.setCategory(category);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (project != null) {
			project = DaoFactory.getProjectDao().addProject(project);
			response = "listProject";
		}

		req.getRequestDispatcher(response).forward(req, resp);
	}
}
