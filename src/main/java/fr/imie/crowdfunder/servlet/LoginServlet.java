package fr.imie.crowdfunder.servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.imie.crowdfunder.dao.DaoFactory;
import fr.imie.crowdfunder.entity.User;

@WebServlet(urlPatterns = "/login")
public class LoginServlet extends HttpServlet {

	private static final long serialVersionUID = -8405626354239284282L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		req.getRequestDispatcher("login.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String responseServlet = "listProject";

		if (req.getSession().getAttribute("currentUser") == null) {
			String pseudo = req.getParameter("pseudo");
			String password = req.getParameter("password");
			List<User> listUser = DaoFactory.getUserDao().getAllUser();

			if (listUser != null) {
				listUser.forEach(user -> {
					if (user.getPseudo().equals(pseudo) && user.getPassword().equals(password)) {
						req.getSession().setAttribute("currentUser", user);
					}
				});

				if (req.getSession().getAttribute("currentUser") == null) {
					responseServlet = "register";
				}
			}

			resp.sendRedirect(responseServlet);
		}
	}
}