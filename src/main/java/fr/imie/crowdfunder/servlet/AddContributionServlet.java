package fr.imie.crowdfunder.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.imie.crowdfunder.dao.DaoFactory;
import fr.imie.crowdfunder.entity.Project;

@WebServlet(urlPatterns = "/addContribution")
public class AddContributionServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String response = "home";

		try {
			long projectId = Long.parseLong(req.getParameter("projectId"));
			Project project = DaoFactory.getProjectDao().findProject(projectId);
			req.setAttribute("project", project);

			response = "addContribution.jsp";
		} catch (Exception e) {
			e.printStackTrace();
		}
		req.getRequestDispatcher(response).forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Project project = null;
		String responseServlet = "home";

		try {
			Float contribution = Float.parseFloat(req.getParameter("contributionActual"));
			long projectId = Long.parseLong(req.getParameter("projectId"));
			project = DaoFactory.getProjectDao().findProject(projectId);

			project.setContributionActual(project.getContributionActual() + contribution);
			DaoFactory.getProjectDao().updateProject(project);

			responseServlet = "listProject";
		} catch (Exception e) {
			e.printStackTrace();
		}

		req.getRequestDispatcher(responseServlet).forward(req, resp);
	}
}
