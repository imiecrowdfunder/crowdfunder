package fr.imie.crowdfunder.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.imie.crowdfunder.dao.DaoFactory;
import fr.imie.crowdfunder.entity.User;

@WebServlet(urlPatterns = "/register")
public class AddUserServlet extends HttpServlet {

	private static final long serialVersionUID = 9036768037357808006L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		RequestDispatcher rd = req.getRequestDispatcher("addUser.jsp");
		rd.forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String responseServlet = "register";
		User user = null;

		try {
			user = new User();
			user.setPseudo(req.getParameter("pseudo"));
			user.setFirstName(req.getParameter("firstName"));
			user.setLastName(req.getParameter("lastName"));
			user.setPassword(req.getParameter("password"));
			user.setEmail(req.getParameter("email"));
			DaoFactory.getUserDao().addUser(user);
			responseServlet = "login";
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (user != null) {

		}

		req.getRequestDispatcher(responseServlet).forward(req, resp);
	}

}
