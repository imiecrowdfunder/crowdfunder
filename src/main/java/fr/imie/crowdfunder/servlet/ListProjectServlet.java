package fr.imie.crowdfunder.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.imie.crowdfunder.dao.DaoFactory;
import fr.imie.crowdfunder.entity.Category;
import fr.imie.crowdfunder.entity.Project;

@WebServlet(urlPatterns = "/listProject")
public class ListProjectServlet extends HttpServlet {

	private static final long serialVersionUID = -4423060934121852966L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<Category> listCategory = DaoFactory.getCategoryDao().getAllCategory();

		if (listCategory == null) {
			listCategory = new ArrayList<Category>();
		}

		req.setAttribute("listCategory", listCategory);

		List<Project> listProject = DaoFactory.getProjectDao().getAllProject();

		if (listProject != null) {
			if (req.getParameter("categoryId") != null) {
				try {
					long categoryId = Long.parseLong(req.getParameter("categoryId"));
					final Category category = DaoFactory.getCategoryDao().findCategory(categoryId);

					if (category != null) {
						List<Project> listP = new ArrayList<Project>();

						listProject.forEach(project -> {
							if (project.getCategory().equals(category)) {
								listP.add(project);
							}
						});

						listProject = listP;
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} else {
			listProject = new ArrayList<Project>();
		}

		req.setAttribute("listProject", listProject);

		req.getRequestDispatcher("listProject.jsp").forward(req, resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		this.doGet(req, resp);
	}
}
