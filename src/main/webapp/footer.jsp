<footer class="footer fixed-bottom">
	<div id="copyright" class="container mt-2 text-sm-center">
		<span class="text-muted text-sm-center">Copyright 2017 -
			Developed by GESK Team</span>
	</div>
</footer>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"
	integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
	crossorigin="anonymous"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js"></script>
<script src="https://use.fontawesome.com/179691dd82.js"></script>
</body>
</html>