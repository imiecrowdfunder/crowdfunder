<%@page import="org.apache.taglibs.standard.tag.rt.core.ImportTag"%>
<%@page import="java.util.List"%>
<%@page import="fr.imie.crowdfunder.entity.Category"%>
<%@page import="fr.imie.crowdfunder.entity.User"%>
<%@page import="fr.imie.crowdfunder.entity.Project"%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="fr.imie.crowdfunder.entity.User"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Crowd GESK</title>
<link
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css"
	rel="stylesheet">
<link
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
	rel="stylesheet">
<link href="css/styles.css" rel="stylesheet">
<link rel="icon" href="images/crowdfavicon.ico" />
</head>

<body>
	<header>
		<nav
			class="navbar navbar-toggleable-md navbar-inverse bg-inverse fixed-top navbar-style">
			<button class="navbar-toggler navbar-toggler-right button-menu"
				type="button" data-toggle="collapse"
				data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup"
				aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<a class="navbar-brand logo" href="/CrowdFunder/home"> <img
				src="images/crowdlogo.png" alt="Logo" height="40px" width="40px">

			</a>
			<div class="collapse navbar-collapse" id="navbarNavAltMarkup">
				<div class="navbar-nav">
					<a class="nav-item nav-link" href="listProject">Projects</a>
					<%
						User currentUser = (User) session.getAttribute("currentUser");
						if (currentUser != null) {
					%>

					<a class="nav-item nav-link" href="/CrowdFunder/showUser">Profile</a>
					<a class="nav-item nav-link" href="/CrowdFunder/logout">Logout</a>
					<%
						} else {
					%>
					<a class="nav-item nav-link" href="/CrowdFunder/login">Login</a>
					<%
						}
					%>
				</div>
			</div>
		</nav>
	</header>