<%@include file="/header.jsp"%>

<h3 class="text-sm-center ml-auto mr-auto h-title">Projects</h3>

<div class="container">
	<div class="dropdown">
		<button class="btn btn-secondary dropdown-toggle" type="button"
			id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
			aria-expanded="false">Choose a Category</button>
		<div id="listCategory" class="dropdown-menu"
			aria-labelledby="dropdownMenuButton">
			<c:forEach items="${listCategory}" var="category">
				<a class="dropdown-item"
					href="listProject?categoryId=<c:out
						value="${category.categoryId}" />">
					<c:out value="${category.name}" />
				</a>
			</c:forEach>
		</div>
	</div>

	<table class="table table-hover table-bordered">
		<thead class="thead-inverse">
			<tr class="custom-th">
				<th>Name</th>
				<%
					if (currentUser != null) {
				%>
				<th>Action</th>
				<%
					}
				%>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${listProject}" var="project">
				<tr>
					<td><a href="showProject?projectId=${project.projectId}">
							<c:out value="${project.name}" />
					</a></td>
					<%
						if (currentUser != null) {
					%>
					<c:if test="${project.user.userId == currentUser.userId}">
						<td class="text-sm-center"><a
							href="editProject?projectId=${project.projectId}">
								<button class="btn btn-primary">
									<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
								</button>
						</a> <a href="removeProject?projectId=${project.projectId}">
								<button class="btn btn-danger">
									<i class="fa fa-minus-circle" aria-hidden="true"></i>
								</button>
						</a></td>
					</c:if>
					<%
						}
					%>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	<%
		if (currentUser != null) {
	%>
	<a href="addProject">
		<button class="btn btn-primary">Add a new Project</button>
	</a>
	<%
		}
	%>
</div>

<%@ include file="footer.jsp"%>
