<%@include file="/header.jsp"%>

<h3 class="text-sm-center ml-auto mr-auto h-title">Contribution</h3>
<div class="container">
	<form action="addContribution" method="post">
		<div class="form-group col-sm-4 offset-sm-4">
			<input type="hidden" name="projectId"
				value="<c:out value="${project.projectId}" />" /> <label
				for="contribution">Choose Contribution</label> <input
				id="contributionActual" name="contributionActual" type="number" />

			<button class="btn btn-primary">Contribute</button>
		</div>
	</form>
</div>

<%@include file="/footer.jsp"%>