<%@include file="/header.jsp"%>

<h3 class="text-sm-center ml-auto mr-auto h-title">Create a new
	project</h3>

<form action="addProject" method="post" class="row container m-auto">

	<%-- NAME --%>
	<div class="form-group col-sm-4 offset-sm-4">
		<label for="name"><br>Name :</label> <input type="text"
			class="form-control" id="name" placeholder="Enter Your Project Name"
			name="name">
	</div>

	<%-- DESCRIPTION --%>
	<div class="form-group col-sm-4 offset-sm-4">
		<label for="description">Description :</label> <input type="text"
			class="form-control" id="description"
			placeholder="Enter a description" name="description" required
			name="description">
	</div>

	<%-- CATEGORY --%>
	<div class="form-group col-sm-4 offset-sm-4">
		<label class="mr-sm-2" for="inlineFormCustomSelect">Choose a
			Category :</label> <select name="categoryId"
			class="custom-select mb-2 mr-sm-2 mb-sm-0"
			id="inlineFormCustomSelect">
			<c:forEach items="${listCategory}" var="category">
				<option value="<c:out
							value="${category.categoryId}" />"><c:out
						value="${category.name}" /></option>
			</c:forEach>
		</select>
	</div>
	<%-- EXPECTED CONTRIBUTION --%>
	<div class="form-group col-sm-4 offset-sm-4">
		<label for="contributionExpected">Expected Contribution :</label> <input
			type="number" class="form-control" id="contributionExpected"
			placeholder="Enter a Expected Contribution"
			name="contributionExpected" required>
	</div>

	<%-- END DATE --%>
	<div class="form-group col-sm-4 offset-sm-4">
		<label for="endDate">End date : </label> <input type="date"
			class="form-control" id="dateEnd" placeholder="Enter a End Date"
			name="dateEnd" required>
	</div>

	<div class="form-group col-sm-4 offset-sm-4">
		<button type="submit"
			class="btn btn-primary col-sm-14 offset-sm-10 m-auto btn-register">Add</button>
	</div>
</form>

<%@ include file="/footer.jsp"%>