<%@ include file="/header.jsp"%>

<h3 class="text-sm-center ml-auto mr-auto h-title">Edit Profile</h3>

<form action="editUser" method="post" class="row container m-auto">
	<div class="form-group col-sm-4 offset-sm-4">
		<label for="pseudo">Pseudo</label> <input type="text"
			value="${currentUser.pseudo}" class="form-control" id="pseudo"
			placeholder="Enter Your Pseudo" name="pseudo">
	</div>

	<div class="form-group col-sm-4 offset-sm-4">
		<label for="firstname">First Name</label> <input type="text"
			value="${currentUser.firstName}" class="form-control" id="firstName"
			placeholder="Enter Your First Name" name="firstName">
	</div>

	<div class="form-group col-sm-4 offset-sm-4">
		<label for="lastname">Last Name</label> <input type="text"
			value="${currentUser.lastName}" class="form-control" id="lastName"
			placeholder="Enter Your Last Name" name="lastName" required>
	</div>

	<div class="form-group col-sm-4 offset-sm-4">
		<label for="password">Password</label> <input type="password"
			value="${currentUser.password}" class="form-control" id="password"
			placeholder="Enter Your password" name="password" required>
	</div>
	<div class="form-group col-sm-4 offset-sm-4">
		<label for="lastname">Email</label> <input type="text"
			value="${currentUser.email}" class="form-control" id="email"
			placeholder="Enter Your Email" name="email" required>
	</div>
	<div class="form-group col-sm-4 offset-sm-4">
		<button type="submit"
			class="btn btn-primary col-sm-14 offset-sm-10 m-auto btn-register">Save
			Changes</button>
	</div>
</form>

<%@ include file="/footer.jsp"%>
