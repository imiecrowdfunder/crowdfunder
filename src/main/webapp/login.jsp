<%@ include file="/header.jsp"%>

<h3 class="text-sm-center ml-auto mr-auto h-title">Login</h3>

<form action="login" method="post" class="row container m-auto">
	<div class="form-group col-sm-4 offset-sm-4">
		<label for="pseudo"> Pseudo</label> <input type="text"
			class="form-control" id="pseudo" name="pseudo" placeholder="pseudo">
	</div>

	<div class="form-group col-sm-4 offset-sm-4">
		<label for="password">Password</label> <input type="password"
			class="form-control" id="password" name="password"
			placeholder="Password">
	</div>

	<div class="form-group col-sm-4 offset-sm-4">
		<button type="submit"
			class="btn btn-warning col-sm-6 offset-sm-3 m-auto btn-login">
			Log in</button>
	</div>
</form>

<div class="text-sm-center mt-2">
	<p>Don't have any account yet ?</p>
	<a href="register">
		<button type="submit"
			class="btn btn-primary col-sm-2 offset-sm-10 m-auto btn-register">Register</button>
	</a>
</div>

<%@ include file="footer.jsp"%>
