<%@include file="/header.jsp"%>

<h3 class="text-sm-center ml-auto mr-auto h-title">Project</h3>
<div class="container">
	<form action="editProject" method="post" class="row container m-auto">
		<input type="hidden" name="projectId"
			value="<c:out value="${project.projectId}" />" />

		<%-- NAME --%>
		<div class="form-group col-sm-4 offset-sm-4">
			<label for="name"><br>Name :</label>
			<p class="form-control">
				<c:out value="${project.name}" />
			</p>
		</div>

		<%-- DESCRIPTION --%>
		<div class="form-group col-sm-4 offset-sm-4">
			<label for="description">Description :</label> <input type="text"
				class="form-control" id="description"
				placeholder="Enter a description" name="description" required
				value="<c:out value="${project.description}" />">
		</div>

		<%-- CATEGORY --%>
		<div class="form-group col-sm-4 offset-sm-4">
			<label class="mr-sm-2" for="inlineFormCustomSelect">Choose a
				Category :</label> <select name="categoryId"
				class="custom-select mb-2 mr-sm-2 mb-sm-0"
				id="inlineFormCustomSelect">
				<option selected
					value="<c:out
							value="${currentCategory.categoryId}" />"><c:out
						value="${currentCategory.name}" /></option>
				<c:forEach items="${listCategory}" var="category">
					<c:if test="${currentCategory.categoryId != category.categoryId}">
						<option value="<c:out
							value="${category.categoryId}" />"><c:out
								value="${category.name}" /></option>
					</c:if>
				</c:forEach>
			</select>
		</div>

		<div class="form-group col-sm-4 offset-sm-4">
			<button type="submit"
				class="btn btn-primary col-sm-14 offset-sm-10 m-auto btn-register">Edit</button>
		</div>
	</form>
</div>

<%@include file="/footer.jsp"%>