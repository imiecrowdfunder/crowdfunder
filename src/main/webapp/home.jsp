<%@ include file="/header.jsp"%>

<div class="container">
	<img src="images/crowdgesk.jpg" width="100%">

	<h3 id="title_home" class="text-sm-center mt-2 mb-2">Last Projects
		Added</h3>

	<div class="row">
		<c:forEach items="${listProject}" var="project">
			<div class="col-md-4 col-sm-12">
				<div class="card">
					<h3 class="card-header custom-card-header">
						<c:out value="${project.name}" />
					</h3>
					<div class="card-block">
						<h4 class="card-title">
							<c:out value="${project.category.name}" />
						</h4>
						<p class="card-text">
							<c:out value="${project.description}" />
						</p>
						<a href="showProject?projectId=${project.projectId}"
							class="btn btn-primary btn-theme">See Details</a>
					</div>
				</div>
			</div>
		</c:forEach>
	</div>
</div>

<%@ include file="footer.jsp"%>