<%@ include file="/header.jsp"%>

<h3 class="text-sm-center ml-auto mr-auto h-title">
	<c:out value="${project.name}" />
</h3>

<div class="container">
	<table class="table">
		<tr>
			<th>Category Name</th>
			<th>Expected Contribution</th>
		</tr>
		<tr>
			<td><c:out value="${project.category.name}" /></td>
			<td><c:out value="${project.contributionExpected}" /></td>
		</tr>
		<tr>
			<th>Created By</th>
			<th>Current Contribution</th>
		</tr>
		<tr>
			<td><c:out value="${project.user.pseudo}" /></td>
			<td><c:out value="${project.contributionActual}" /></td>
		</tr>
		<tr>
			<th>Date of Creation</th>
		</tr>
		<tr>
			<td><fmt:formatDate pattern="dd/MM/yyyy"
					value="${project.dateBegin}" /></td>
		</tr>
	</table>

	<div class="project-description">
		<p>
			<c:out value="${project.description}" />
		</p>
	</div>

	<div class="contributor-list">
		<ul>
			<c:forEach items="${listContributor}" var="user">
				<li><c:out value="${user.pseudo}" /></li>
			</c:forEach>
		</ul>
	</div>
	<%
		Project project = (Project) request.getAttribute("project");
		if (currentUser != null && project.getUser().getUserId() != currentUser.getUserId()) {
	%>
	<a href="addContribution?projectId=${project.projectId}">
		<button class="btn btn-primary">Contribute</button>
	</a>
	<%
		}
	%>
</div>

<%@ include file="footer.jsp"%>
