<%@include file="/header.jsp"%>

<h3 class="text-sm-center ml-auto mr-auto h-title">Register</h3>

<form action="register" method="post" class="row container m-auto">

	<div class="form-group col-sm-4 offset-sm-4">
		<label for="name">Pseudo</label> <input type="text"
			class="form-control" id="pseudo" placeholder="Enter Your Pseudo">
	</div>

	<div class="form-group col-sm-4 offset-sm-4">
		<label for="pseudo">Pseudo</label> <input type="text"
			class="form-control" id="pseudo" placeholder="Enter Your Pseudo"
			name="pseudo" required>
	</div>

	<div class="form-group col-sm-4 offset-sm-4">
		<label for="firstName">First Name</label> <input type="text"
			class="form-control" id="firstName"
			placeholder="Enter Your First Name" name="firstName" required>
	</div>

	<div class="form-group col-sm-4 offset-sm-4">
		<label for="lastName">Last Name</label> <input type="text"
			class="form-control" id="lastName" placeholder="Enter Your Last Name"
			name="lastName" required>
	</div>

	<div class="form-group col-sm-4 offset-sm-4">
		<label for="password">Password</label> <input type="password"
			class="form-control" id="password" placeholder="Enter Your password"
			name="password" required>
	</div>
	<div class="form-group col-sm-4 offset-sm-4">
		<label for="lastname">Email</label> <input type="text"
			class="form-control" id="email" placeholder="Enter Your Email"
			name="email" required>
	</div>
	<div class="form-group col-sm-4 offset-sm-4">
		<button type="submit"
			class="btn btn-primary col-sm-6 offset-sm-3 m-auto btn-register">Register</button>
	</div>
</form>

<%@ include file="/footer.jsp"%>