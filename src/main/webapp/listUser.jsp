<%@ include file="/header.jsp"%>

<h3 class="text-sm-center ml-auto mr-auto h-title">Users</h3>

<div class="container">
	<table class="table table-hover table-bordered">
		<thead class="thead-inverse">
			<tr class="custom-th">
				<th>#</th>
				<th>Pseudo</th>
				<th>Firstname</th>
				<th>Lastname</th>
				<th>Email</th>
				<th>Password</th>
				<%
					if (currentUser != null) {
				%>
				<th>Remove</th>
				<%
					}
				%>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${listUser}" var="user">
				<tr>
					<td><c:out value="${user.id}" /></td>
					<td><c:out value="${user.pseudo}" /></td>
					<td><c:out value="${user.firstname}" /></td>
					<td><c:out value="${user.lastname}" /></td>
					<td><c:out value="${user.email}" /></td>
					<td><c:out value="${user.password}" /></td>
					<%
						if (currentUser != null) {
					%>
					<td class="text-sm-center"><a href="removeUser?id=${user.id}">
							<button class="btn btn-danger">
								<i class="fa fa-minus-circle" aria-hidden="true"></i>
							</button>
					</a></td>
					<%
						}
					%>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>

<%@ include file="footer.jsp"%>