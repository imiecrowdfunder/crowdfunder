<%@ include file="/header.jsp"%>

<h3 class="text-sm-center ml-auto mr-auto h-title">My Profile</h3>

<div class="row container m-auto">

	<div class="form-group col-sm-4 offset-sm-4">
		<p>Pseudo</p>
		<p>
			<c:out value="${currentUser.pseudo}" />
		</p>
	</div>

	<div class="form-group col-sm-4 offset-sm-4">
		<p>Firstname</p>
		<p>
			<c:out value="${currentUser.firstName}" />
		</p>
	</div>

	<div class="form-group col-sm-4 offset-sm-4">
		<p>Lastname</p>
		<p>
			<c:out value="${currentUser.lastName}" />
		</p>
	</div>
	<div class="form-group col-sm-4 offset-sm-4">
		<p>Email</p>
		<p>
			<c:out value="${currentUser.email}" />
		</p>
	</div>
	<div class="form-group col-sm-4 offset-sm-4">
		<p>Password</p>
		<p>
			<c:out value="${currentUser.password}" />
		</p>
	</div>
	<a class="col-sm-4 offset-sm-4" href="editUser">
		<button
			class="btn btn-primary col-sm-14 offset-sm-10 m-auto btn-theme">Edit
			Profile</button>
	</a>
</div>

<%@ include file="/footer.jsp"%>